---
title: "Mentoring"
slug: mentoring
status: draft

---

At the start of winter semester 2021/22, the Fachgruppe Computer Science will offer a mentoring program for the first time. We'd like to give you, the first semester students of Applied Computer Science, the opportunity to get a better introduction to computer science, as well as making establishing and maintaining new contacts easier.
Participation is voluntary.

## Plan
Nach dem Ende der [O-Phase](slug:ophase) werden wir Menteegruppen bilden, die sich aus einigen wenigen Erstsemestern (Mentees) und 1-2 Mentor:innen zusammensetzen. Mentor:innen sind dabei andere Studierende der angewandten Informatik (Semster 3 und höher).  
Diese Gruppen werden über eure ersten beiden Semester bestehen und ihr erhaltet die Möglichkeit, euch alle zwei Wochen zu treffen, um euch gemütlich zusammenzusetzen und auszutauschen.

After the end of [orientation week](slug:ophase), we will create mentee groups consisting of a few first semester students (mentees) and 1-2 mentors. Mentors are other Applied Computer Science students in at least their third semester.
These groups will stay intact over your first two semesters and you have the opportunity to meet every two weeks to sit together in an informal atmosphere and exchange your experiences.

## Matching
Matching mentors and mentees is a central step at the beginning of the program with the goal of creating groups where you feel well. We'll give you details in the next few weeks.

## Mentoring events
Event details will come later.

## I'd like to participate
Do you want to participate as a mentor or mentee? If so, write us an e-mail at [fachgruppe@informatik.uni-goettingen.de](mailto:fachgruppe@informatik.uni-goettingen.de)
