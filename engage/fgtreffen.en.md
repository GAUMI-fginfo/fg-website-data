---
title: "Fachgruppen Meetups"
slug: fgtreffen
tags: engage, event, fachgruppe

---

At our weekly meetups we discuss current topics, plan events and talk about studying and study quality in an informal atmosphere. We are always happy when we find new interested people, so come by, even if you "just want to check it out" or meet nice people!

Currently we meet on Tuesdays in odd numbered weeks at 18:00 in [IfI room 0.101](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101). (A bit less regularly during the semester break).
Once every semester there is a [full assembly of the computer science student body](slug:fgvv), where we report what has been going on and typically talk about general topics of studying where everyone can have a say.


