---
title: "Fachgruppen&shy;treffen"
slug: fgtreffen
tags: engage, event, fachgruppe

relevant: 0
---

Auf Fachgruppentreffen besprechen wir in geselliger Runde die aktuell anstehenden Themen, planen Veranstaltungen und unterhalten uns übers Studium und Studienqualität. Wir freuen uns immer über Interessierte, also komm vorbei, auch wenn du "nur mal reinschnuppern" oder nette Leute kennenlernen willst!

Momentan treffen wir uns in ungeraden Wochen dienstags um 18:00 Uhr im [IfI Raum 0.101](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101). (In den Semesterferien etwas unregelmäßiger.)
Einmal im Semester gibt es außerdem eine [Fachgruppenvollversammlung](slug:fgvv), da berichten wir, was sich so getan hat und sprechen typischerweise über allgemeine Themen des Studiums bei denen alle gut mitreden können.
