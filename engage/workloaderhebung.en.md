---
title: "Workloaderhebung zweites Semester"
slug: workloaderhebung
tags: engage
---

**This page is not translated, because it only targets Bachelor students.**

## Worum geht es?

:::warning
Wir bekommen seit einigen Jahren mit, dass viele Zweitsemester es nicht schaffen gleichzeitig die Veranstaltungen Mafia II und APP wie vorgesehen zu absolvieren. Viele berichten von einer hohen Belastung, schreiben Mafia II auf dem Zweittermin oder absolvieren das APP erst im 4. Semester. Wir wollen nun wissen, ob das zweite Semester tatsächlich zu voll ist und entlastet werden müsste. Um das herauszufinden muss eine "Workloaderhebung" durchgeführt werden. **Und dafür brauchen wir eure Unterstützung als aktuelle Zweitsemester, um zu erfassen, wie viel Zeit ihr tatsächlich braucht.**
:::

## Mitmachen

Super, dass du mitmachen willst! Füll zunächst mal diese kurze Umfrage aus. Wie du im Laufe des Semesters deine Daten einreichst, findest du weiter unten. 

Hier der Link zur Anmeldung: https://survey.academiccloud.de/index.php/577985?newtest=Y

## Was erwartet dich? 

Damit wir alle nötigen Daten für die Workloaderhebung bekommen:

- Du schreibst immer auf, wenn du etwas für eine Veranstaltung machst
    - falls du das mal vergisst, vermerkst du an welchem Tag, du es vergessen hast und schätzt die Zeiten
- Einmal pro Woche musst du deine Zeiten bei uns abgeben. Wir haben außerdem jede Woche folgende drei Fragen:
    - Wieviel von den vorgesehenen Aufgaben der Woche hast du bearbeitet?
    - Wieviel Prozent der Wochenaufgaben hast du selbst gelöst? Das heißt NICHT von Kommilitonen abgeschrieben, aus dem Internet kopiert oder ChatGPT, GitHub Copilot, o.Ä. lösen lassen. Diese Daten geben wir natürlich nicht weiter, für eine gute Einschätzung sind sie aber wichtig. Damit sie angemessen geschützt sind, erfolgt die Abgabe der Wochenzeiten nicht unter einem Klarnamen (pseudonym).
    - Wie stressig war die vergangene Woche für dich?
- Um die Daten besser in den Kontext setzen zu können, bitten wir dich zu Beginn einen Überblick über deinen Semesterplan zu geben und am Ende über die Prüfungen, an denen du teilnimmst.

## Datenformat / Tools
Natürlich müssen wir eure Daten auch lesen können. Ihr habt mehrere Möglichkeiten:

1. CSV oder Tabellenkalkulation mit folgenden Spalten:
    - `Modul` (z.B. `APP`, `Mafia2`, `Info2` etc.)
    - `Start` im Format `TT.MM.YYYY hh:mm:ss`
        - in Libreoffice & Excel kann man mit `STRG`+`;` das Datum und mit `STRG`+`SHIFT`+`;` die aktuelle Zeit einfügen)
    - `Ende` im Format `TT.MM.YYYY hh:mm:ss`
    - `Geschätzt` (`y` oder `n`)
2. Einen Timetracker der mindestens diese Informationen erfasst. Das tut z.B. Toggl, dass auch einen csv-Export hat. Die Nutzung ist aber freiwillig. **Wichtig ist, dass wir mit den enstehenden Daten etwas anfangen können**, möglichst ohne uns zu viel Arbeit zu machen! Zu einer Anleitung für Toggl geht es [hier](https://pad.gwdg.de/s/CNT3DDuWi#) (coming soon)
3. Ein selbstgeschriebenes Tool ist zum Beispiel [hier](https://gitlab.gwdg.de/n.lackschewitz/worktime) zu finden.
