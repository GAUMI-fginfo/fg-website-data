---
title: "Minecraft LAN-Party"
slug: minecraft
lang: de

relevant:
  end: 2025-02-03
  prio: 3

---

- **Wann?** Start ist am 31.01.2025 um 20:00 und die LAN geht das ganze Wochenende.
    - Ihr könnt gerne etwas früher kommen, damit wir um 20:00 gemeinsam den Server einweihen können!
- **Wer?** Alle, die wollen - egal ob du vor 10 Jahren oder 10 Minuten zuletzt Minecraft gespielt hast.
- **Wo?** [Raum -1.110 im Institut für Informatik](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110)
    - Hinweis: Wir spielen ausschließlich vor Ort, damit es auch zu richtigem LAN-Feeling kommt.

![Plakat](file:Minecraft_LAN_0125.png)

## Anleitung & Regeln
Wir spielen in den Rechnerräumen. Im SCRATCH-Ordner wird ein Skript sein, mit dem man sich auf einfache Art und Weise Minecraft installieren kann.

Beim ersten Einloggen in den Server müsst ihr euch ein Passwort geben, indem ihr `/register <passwort>` eingebt. Danach seid ihr angemeldet. Solltet ihr danach gefragt werden, könnt ihr euch mit `/login <passwort>` neu einloggen.

Wir wollen, dass niemandem der Spaß am Spielen genommen wird. Die Standards des gegenseitigen Umgangs sind im Spiel nicht niedriger als außerhalb. Also:

1. Habt Spaß und helft mit, dass eure Mitspielenden auch Spaß haben.
2. Seid nett zueinander und geht gut miteinander um.
3. Spieler:innen können sich auf dem Server nicht gegenseitig verletzen (PvP ist aus).

Minecraft ist ein Spiel mit nahezu endlosen Möglichkeiten und es gibt viel Wege Spaß damit zu haben. Deshalb wollen wir euch nicht weiter einschränken, aber stehen bei Fragen oder Problemen vor und wärend der Veranstaltung jederzeit zur Verfügung.

## Über den Server
Wir spielen Vanilla Minecraft d.h. ohne größere Modifikationen.
Der Server läuft aus Datenschutzgründen im Offline-Modus, damit eure persönlichen Daten nicht an Microsoft übertragen werden.

**Technische Details:**
Wir spielen auf einem Fabric Server mit folgenden Mods:
- Lithium
- Async
- EasyAuth
- Carpet
- ChunkyBorder

Client-side gibt es zusätzlich noch folgende Mods:
- Sodium
- Sodium Extra
- Bobby 

