---
title: "Zweitifahrt 2025"
slug: zweitifahrt

---
![Zweitfahrt2025 Plakat](file:zweitifahrt2025Plakat.png)

## register here
**https://survey.academiccloud.de/index.php/899684?lang=de**

## ALL YOU NEED TO KNOW!!!!
:::success
After one semester of studying, you still don't really have your head in the game? Or are you already fully settled and just want to meet new people? No matter where you are right now - the second semester trip of the Computer Science and Data Science Fachgruppe is the perfect start to the second semester!
Before the APP and exercise sheets are due, we spend a relaxed weekend in the Harz Mountains. In addition to a rally, games and a (pub) quiz, you can also look forward to a cozy campfire evening.
Join us and start your second semester off right!
:::
### When?
- 25.04.2025 starting at 16:00 Arrival 
- 27.04.2025 until 11:00 Departure

### Where?
| Jugend- und Wanderheim Wildemann 
| Im Schwarzewald 21
| 38709 Wildemann

### How?
:::info
Arrival/departure is to be organized privately (with public transport you need ~2.5 hours)
:::

### Schedule
- The entire site is at our disposal!
- We are planning a pubquiz, campfire, a rally and more
- Accommodation and catering are organized by the FG and are included in the price

### Costs
- 38€ per person for accommodation, food and tourist tax
    - Food will be vegan
- 5€ for bed linen per person in cash on site (these costs are not included in the 38€)
    - The rental of bed linen is voluntary, you can also bring your own from home 

### Payment

After successfully registering for the trip, you will receive a confirmation email with further information on how to make the transfer

### Other 
If you want to connect for the journey and other things, here is a signal group:
https://signal.group/#CjQKIEejRLhod396jE2DIBiGIRZFl6WfXTNFQhfQQDTymFEnEhBvLZaGzNUJ4DMd1w96-dIq

