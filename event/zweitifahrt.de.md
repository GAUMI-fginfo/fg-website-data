---
title: "Zweitifahrt 2025"
slug: zweitifahrt

relevant:
  end: 2025-04-27
  prio: 0
---
![Zweitfahrt2025 Plakat](file:zweitifahrt2025Plakat.png)

## Hier geht's zur Anmeldung
**https://survey.academiccloud.de/index.php/899684?lang=de**

## ALLES, WAS DU WISSEN MUSST!!!!
:::success
Nach einem Semester Studium hast du noch nicht so richtig den Durchblick? Oder bist du bereits voll angekommen und hast einfach Lust, neue Leute kennenzulernen? Ganz egal wo du gerade stehst - die Zweitifahrt der Informatik und Data Science ist der perfekte Start ins zweite Semester!
Bevor das APP und die Zettelabgaben anstehen, verbringen wir ein entspanntes Wochenende im Harz. Neben einer Rallye, Spielen und einem (Pub-)Quiz kannst du dich auch auf einen gemütlichen Lagerfeuerabend freuen. 
Sei dabei und beginne dein zweites Semester richtig!

:::
### Wann?
- 25.04.2025 ab 16 Uhr Anreise 
- 27.04.2025 bis 11 Uhr Abreise

### Wo?
| Jugend- und Wanderheim Wildemann 
| Im Schwarzewald 21
| 38709 Wildemann

### Wie?
:::info
An-/Abreise ist privat zu organisieren (Mit Öffis benötigt man ~2,5 Stunden)
:::

### Programm
- Uns steht das gesamte Gelände zur Verfügung!
- Geplant sind ein PubQuiz, Lagerfeuer, eine Harz-Rallye und mehr
- Unterkunft und Versorgung werden von der FG organisiert und sind im Preis inklusive

### Kosten
- 38€ pro Person für Übernachtung, Essen und Kurtaxe
    - Essen wird Vegan sein
- 5€ für Bettwäsche p.P in Bar vor Ort (**Diese Kosten sind nicht in den 38€ enthalten**)
    - Die Ausleihung von Bettwäsche ist freiwillig, ihr könnt auch selber welche von zuhause mitbringen

### Bezahlung

Nach der erfolgreichen Anmeldung zur Fahrt, erhaltet ihr eine Bestätigungsemail mit weiteren Informationen zur Art der Überweisung.

### Sonstiges 
Wenn ihr euch für die Anfahrt und sonstiges connecten wollt hier eine Signalgruppe:
https://signal.group/#CjQKIEejRLhod396jE2DIBiGIRZFl6WfXTNFQhfQQDTymFEnEhBvLZaGzNUJ4DMd1w96-dIq


