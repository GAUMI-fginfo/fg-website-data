---
title: "Nordcampus&shy;Glühen"
slug: nordcampusgluehen

tags: event
---

The NordcampusGlühen is a joint project of the North Campus Fachschaften and Fachgruppen that takes place in the winter semester. We are in charge of organizing it and it took place for the first time in 2022.

## Invitation

The NordcampusGlühen will take place on Fri. 29.11.2024 from 4 pm at the [container buildings at the bus stop Tammannstraße](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.101&lang=en). As a joint project of the North Campus Fachschaften and Fachgruppen, there will be Glühwein (mulled wine), punch and much more at a few booths. Everyone and especially north campus students are invited to stop by.

:::warning
Please bring your own cup for hot beverages!
:::

![Nordcampusglühen Poster 2024](file:nordcampusgluehen_2024.png)
