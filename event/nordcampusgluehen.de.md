---
title: "Nordcampus&shy;Glühen"
slug: nordcampusgluehen

relevant:
  end: 2024-11-30
  prio: 5
tags: event
---

Das NordcampusGlühen ist ein im Wintersemester stattfindendes Gemeinschaftsprojekt der Nordcampusfachschaften und -Fachgruppen. Es wird federführend von uns organisiert und hat 2022 das erste Mal stattgefunden.

## Einladung

Das NordcampusGlühen findet am Fr. 29.11.2024 ab 16 Uhr [an den Containergebäuden an der Bushaltestelle Tammannstraße](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.101) statt. Als Gemeinschaftsprojekt der Nordcampusfachschaften/-Gruppen wird es an ein paar Ständen Glühwein, Punsch und vieles mehr geben. Alle und vor allem Nordcampus-Studis sind herzlich eingeladen, vorbeizuschauen.

:::warning
Bringt bitte eine eigene Tasse für Heißgetränke mit!
:::

![Nordcampusglühen Plakat 2024](file:nordcampusgluehen_2024.png)
