---
title: "Spiele&shy;abend"
slug: spieleabend
#relevant: 0
---

Ein Spieleabend :game_die: findet jeden **Freitag ab 19:00 Uhr** im Institut für Informatik in [Raum 0.101](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101) statt. Dabei spielen wir verschiedene Karten- und Brettspiele aus unserer umfangreichen Spielesammlung während wir den sanften Klängen von "Diggy Diggy Hole" lauschen :notes:. Es ist immer wieder lustig und macht sehr viel Spaß :tada: mit den netten Menschen :two_women_holding_hands: den Abend zu verbringen. Kommt gerne vorbei.

Wir freuen uns auf euch!

## Häufig gespielete Spiele

Die folgenden Spiele werden häufig auf den Spieleabenden gespielt:

- [Codenames](https://de.wikipedia.org/wiki/Codenames)
- [Dominion](https://de.wikipedia.org/wiki/Dominion_(Spiel)) mit Erweiterugen
- [Wizard](https://de.wikipedia.org/wiki/Wizard_(Spiel))
- [Tichu](https://de.wikipedia.org/wiki/Tichu)
- [Secret Hitler](https://de.wikipedia.org/wiki/Secret_Hitler)
- [Werwolf](https://de.wikipedia.org/wiki/Die_Werw%C3%B6lfe_von_D%C3%BCsterwald)
- [Bang!](https://de.wikipedia.org/wiki/Bang!)
- [Schach](https://de.wikipedia.org/wiki/Schach)
- [Der Widerstand](https://shop.heidelberger-spieleverlag.de/Der-Widerstand)

Diese und viele Weitere Spiele gibt es in dem [Spieleschrank im Fachgruppenraum](slug:fgraum#spieleschrank).
