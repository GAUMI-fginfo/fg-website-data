---
title: "Cookie Talks"
slug: cookie-talks

tags: engage
relevant:
  end: 2025-02-05
  prio: 4
---

:::info
To be always informed, just sign up on the **[Cookie-Talks mail distribution list](https://listserv.gwdg.de/mailman/listinfo/keksseminar)**.
:::

The lecture series of the Fachgruppe Informatik: In a relaxed atmosphere with cookies and drinks. The topics range from purely technical talks, to hands-on topics, to general study-related topics.

[TOC]

:::info
You want to give a Cookie Talk yourself? Just get in touch: [fachgruppe@informatik.uni-goettingen.de](mailto:fachgruppe@informatik.uni-goettingen.de)
A lot of the talks are by and for students, feel free to join us!
:::

## Dates in the winter semester 2024/25

All talks will take place in room [0.101 in the computer science institute](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101), unless otherwise announced.

Topics:

- 4.12. 18:00
    - LaTeX (Alrun), you can find the [LaTeX cheat sheet from the talk here](file:latex-cheatsheet.pdf)
    - [GitLab instead of StudIP](https://gitlab.gwdg.de/j.vondoemming/studip-gitlab-sync) (Jake)
- 11.12. 18:00
    - What's going on in the .git Folder? (Thilo)
    - Overengineering the Bash prompt (Jake)
- 8.1. 18:00
    - University elections: What are they and why? (Tom (Mir Egal) and Robin (Nerdcampus))
    - Cooking (Brahim and Mika)
- 15.1. 18:00
    - Formula Student: Student build Racing Cars (Lukas)
    - VSTs: Making Plugins for Music Software (Luna)
- 22.1. 18:00
    - Programming GPUs with Fragment Shaders (Juno)
    - Lies that you learn during your CS Studies (Lars) [(slides)](file:cookie_talk_lars.pdf)
- 29.1. 18:00
    - ~~Darning Socks (Lin)~~
    - canceled
- 5.2. 18:00
    - Nebula: VPNs but funnier (Nellie)

## Dates in the winter semester 2023/24

All talks will take place in room [0.101 in the computer science institute](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101), unless otherwise announced.

- 15.11.
    1. "Eigenes ChatGPT? Leichter als du denkst! - Eine Einführung in Open Source LLMs" (Lorenz)
    2. "What Every Programmer Should Know About Memory" (Lars)
- 22.11.
    2. "Debugging mit GDB" (Horst)
- 29.11. 
- 06.12.
- 14.12. (Thursday)
    1. Ein vollständiges Projekt entwickeln - von Anfang bis Ende: Am Beispiel der Programmieradventskalender-Website (Jake & Selina)
- 20.12. 
- 10.01.
    1. ~~"Performante Programmierung an Beispielen"~~ (does not take place)
- 17.01. 
- 24.01.
    1. "Sonartechnik und U-Boote" (Alrun)
- 31.01.

![Cookie-Talks Logo](file:keksseminarsticker.png)

## Dates in the summer semester 2023

## Dates in the winter semester 2022/23.

Diese Themen stehen schon fest (es gibt ein oder zwei Vorträge pro Abend):
- 18.01. 18:00 Uhr
  1. Uni-Archivar Dr. Holger Berwinkel gibt eine Einführung in die Archivierung digitaler Dokumente
- 25.01. 18:00 Uhr
  1. Horst erzählt über Mario Kart, von den Grundlagen bis zu fortgeschrittenen Techniken, um die Chancen bei den Mario Kart Turnieren etwas auszugleichen
- 01.02. 18:00 Uhr
  1. Fabio erklärt grundlegende Aspekte von fairer und nachhaltiger Technik
  2. Sergio spricht über die Vorteile von Akkus bezüglich Preis und Naturschutz, und gibt eine Einführung, wie man sich diese Vorteile zunutze machen kann
- 08.02. 18:00 Uhr
  1. Jonas erzählt was zu Klarträumen.
  2. Alrun und Robin diskutieren über das Pro und Contra von Remoulade
- 15.02. 18:00 Uhr
  1. Jan-Philipp bringt uns bei, welche Kriterien für einen Bootsführerschein erfüllt werden müssen, und welche Bedeutung ein Bootsführerschein hat
- 22.02. 18:00 Uhr
  1. Wir lernen von Fabian etwas über die Interaktion von verschiedenen Schriftsystemen, Texten, und Computern
- 08.03. 18:00 Uhr
  1. Lorenz bringt uns einige Programmiersprachen näher, die interessante Konzepte abseits von dem im Uni-Alltag Beigebrachten mitbringen

## Dates during the winter semester 2021/22.
- 01.12. 20:00 Uhr
  1. [Prof. Schmitzer](https://ot.cs.uni-goettingen.de/team_bernhard-schmitzer.html) stellt sich und seine Forschungsgruppe vor
  2. Lorenz zeigt, wie [Node-RED](https://github.com/node-red/node-red) funktioniert und was man damit machen kann
- 15.12. 20:00 Uhr
  1. [Cornelis Kater](https://ckater.de/) spricht über [Open Source in der Lehre](https://opensourcelms.de/)
  2. Asterix spricht über [MateLight](https://jugendgruppe.cccgoe.de/projekte/matelight.html)
- 12.01. 20:00 Uhr
  1. [Prof. Kunkel](https://hps.vi4io.org/about/people/julian_kunkel) stellt sich und seine Forschungsgruppe vor
  2. Conrad spricht über E-Sport Balancing
- 26.01. 20:00 Uhr - Alumni sprechen über den Übergang ins Berufsleben
- 09.02. 20:00 Uhr
  1. Lars spricht über Speicherverwaltung: "What Every Programmer Should Know About Memory" (natürlich auf Deutsch und auch für Einsteiger geeignet)
  2. Jonathan spricht über die Verwaltung von Forschungsmaterial mit Zotero und Obsidian: "Wie man während seiner Bachelor-/Masterarbeit beim Paper Lesen nicht den Überblick verliert"
- 23.02. 20:00 Uhr
  1. [Damian Bast](https://www.linkedin.com/in/damianbast/) spricht über Flutter
  2. ???
- 09.03. 20:00 Uhr
  1. Tobias stellt ein Raspi-Projekt zur Sicherung von Zeitungsartikeln vor
  2. ???
- 23.03. 20:00 Uhr - Fee und Mike: Thementag zu Pandas :panda_face: 

## Dates during the summer semester 2021.
- 16.06.2021 20:00 Uhr
    1. (DE/EN)Circular Economy  (Nele)
    2. (DE) QM-Track (Pascal/Henrik)
- 30.06.2021 20:00 Uhr
   1. (DE) Interaktive Onlineveranstaltungen - ein Erfahrungsbericht der Technik (KIF 49.0 Technik-Team)
   2. (DE) Psychische Gesundheit (Georg)
- 14.07.2021 20.00 Uhr
  1. Vorstellung der Forschungsgruppe von Prof. Fabian Sinz (Prof. Fabian Sinz)
  2. Überraschungsvortrag
- 28.07 2021 20:00 Uhr Präsenz
   1. (DE)  Was tun im Notfall? Auffrischung von Erste Hilfe Kenntnissen (Rosa)
      - Anmeldung erforderlich: fachgruppe@informatik.uni-goettingen.de
      - Masken und Sitzunterlage für Draußen mitbringen.
- 11.08.2021 20:00 Uhr 
   1. (DE/EN) Was kommt nach dem Info Studium? (Alumnis berichten)
- 25.08.2021 20:00 Uhr VORVERLEGT AUF DEN 11.08.2021
- 08.09.2021 20:00 Uhr
    1. (DE) Der Bien und das Imkern (Henrik)
    2.(DE)  Met selbst brauen (Lorenz)
- 22.09.2021 20:00 Uhr
   1. (DE) Das etwas andere SmartApe - SmartBeans (Ole)
   2. (DE) Clustering Cooking Recipes (Anna)
