---
title: "Games Night"
slug: spieleabend
---

There is a Games Night :game_die: every **Friday 7 PM** at the Institute of Computer Science in [Room 0.101](https://lageplan.uni-goettingen.de/?ident=2412_1_EG_0.101). We play different card and board games from our extensive collection, while listening to the soft tunes of "Diggy Diggy Hole" :notes:. It's always great fun :tada: to spend the evening with nice people :two_women_holding_hands:. Feel free to join.

We look forward to seeing you!

## Frequently played games

The following games are played frequently:

- [Codenames](https://en.wikipedia.org/wiki/Codenames)
- [Dominion](https://en.wikipedia.org/wiki/Dominion_(card_game)) with extensions
- [Wizard](https://en.wikipedia.org/wiki/Wizard_(card_game))
- [Tichu](https://en.wikipedia.org/wiki/Tichu)
- [Secret Hitler](https://en.wikipedia.org/wiki/Secret_Hitler)
- [Werewolf](https://en.wikipedia.org/wiki/Mafia_(party_game))
- [Bang!](https://en.wikipedia.org/wiki/Bang!_(card_game))
- [Chess](https://en.wikipedia.org/wiki/Chess)
- [The Resistance](https://en.wikipedia.org/wiki/The_Resistance_(game))


These and many more games can be found in the [game cabinet](slug:fgraum#spieleschrank).
