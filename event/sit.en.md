---
title: "Göttinger Informatiktage (GIT)"
slug: sit

---

The Göttingen Computer Science Days (formerly student computer science days) is a lecture and workshop weekend we host every year in the summersemester. There is always something for everybody.

From friday to sunday we have:
- Lectures and workshops from companys.
- Lectures and workshops from students.
- Social events and games.

:::success
You can get up-to-date informations on our dedicated GIT-Website:
https://git.fg.informatik.uni-goettingen.de/upcoming?lang=en
:::


[![GIT Poster 2023](https://git.fg.informatik.uni-goettingen.de/upcoming/poster/sIT-2023-full.jpg)](https://git.fg.informatik.uni-goettingen.de/upcoming?lang=en)

