---
title: "Programming Advent Calendar"
slug: aoc
link: https://aoc.fg.informatik.uni-goettingen.de/

tags: event
---

:::info
**tl;dr:**
Join our programming advent calendar!
- https://aoc.fg.informatik.uni-goettingen.de/
- Time period: Advent.
- There are prizes to be won.
:::

![Poster 2024](file:AOC_2024_sharepic.png)

Each year, the Fachgruppe Computer Science, together with the Fachgruppe DataScience, the Fachgruppe Mathematics and the Fachschaft of Physics, are running a programming advent calendar with the puzzles from AdventOfCode. Feel free to join and solve the puzzles. You can compete with your fellow students or simply improve your programming skills with practical tasks.

The AdventOfCode is an event that takes place annually since 2015. Every day a small puzzle is published, which you can try to solve. What does this have to do with programming? Well, the puzzle usually consists of thousands of lines and it is better if you write a program that solves the task for you.

Everybody can participate in the AdventOfCode. The programming puzzles target different skills and levels and can be solved in any programming language. People from all over the world use them as speed contests, to prepare for job interviews, for corporate training, as university courses, or simply as practice or to challenge each other.

We create a separate page to track the solved puzzles of our students.

:::success
Our leaderboard can be found on the following page: https://aoc.fg.informatik.uni-goettingen.de/
:::


You do **not** need an account on the [page of AdventOfCode](https://adventofcode.com/).

