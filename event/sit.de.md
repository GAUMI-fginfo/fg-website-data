---
title: "Göttinger Informatiktage (GIT)"
slug: sit
relevant:
  end: 2023-06-12
  prio: 3

---

Die Göttinger Informatiktage (GIT, ehemals sIT) sind ein Vortrags- und Workshopwochenende, die jedes Jahr im Sommersemester von der Fachgruppe Informatik veranstaltet werden. Das Programm ist immer sehr divers und es ist für jede\*n was dabei.

Von Freitag bis Sonntag gibt es ein buntes Programm aus:
- Vorträge und Workshops von Firmen
- Vorträge und Workshops von Alumni und Studierenden
- ein Soziales Rahmenprogramm (Grillen, Jeopardy, etc.)

:::success
Die aktuellsten Informationen findest du auf der GIT-Webseite:
https://git.fg.informatik.uni-goettingen.de/upcoming?lang=de
:::


[![GIT Plakat 2023](https://git.fg.informatik.uni-goettingen.de/upcoming/poster/sIT-2023-full.jpg)](https://git.fg.informatik.uni-goettingen.de/upcoming?lang=de)


## Umbenennung

2022 wurden die studentischen Informatiktage (sIT) zu den Göttinger Informatiktagen (GIT) umbenannt.
