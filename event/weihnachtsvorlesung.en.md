---
title: "Christmas Lecture"
slug: weihnachtsvorlesung

---
Once again the study group informatics invites everyone to join in the Christmas lecture.
Your all welcome to hear Mr. Brosenne, Mr. Pape and Mr. Schmitzer give a lecture each and enjoy Biscuits and hot drinks.

We’re looking forward to seeing you on the 18th of December 2024 in the [Goldschmidtstraße 3-5 MN09](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.102) at 19 o’clock!

P.S: The lectures will probably be held in German and we ask you to bring your own cups.

![Christmas Lecture Poster 2024](file:weihnachtsvorlesung_2024.png)
