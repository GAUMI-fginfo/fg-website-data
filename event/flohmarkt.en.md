---
title: "Flea Market"
slug: flohmarkt

tags: event
---

Are you a new student and need a cooking spoon for your appartment? Or have you been here for a while, have cleaned up, and would like to get rid of a keyboard or a few notepads?

![Flea Market Illustration](file:flohmarktoh.png)

On November 6th, 2022, there will be a flea market at the [Institute of Computer Science](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110), especially for kitchen and study purposes.

- From 14:00 stand construction
- 15:00 to 18:00 flea market

If you'd like to sell or gift something, please let us know beforehand via e-mail: fachgruppe@informatik.uni-goettingen.de.
For larger items that you don't want to transport to here, there will be a bulletin board where you can post offers for desks or similar items. You should arrange transport directly with interested people.

If you only have stuff to give away, you can just come over in the morning with a box of stuff and take it back after 17:00 instead of watching it the whole time. Again, please let us know via e-mail.
