---
title: "Weihnachts&shy;vorlesung"
slug: weihnachtsvorlesung
relevant:
  end: 2024-12-19
  prio: 3

---
Alle Jahre wieder lädt die Fachgruppe Informatik zur Weihnachtsvorlesung ein!
Bei Gebäck und Heißgetränken geben Herr Brosenne, Herr Pape und Herr Schmitzer dieses Jahr Vorträge.

Wir freuen uns euch am 18. Dezember 2024 um 19:00 in der [MN09 Goldschmidtstraße 3-5](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.102) begrüßen zu dürfen!

P.S: Bringt bitte eigene Tassen mit.

![Weihnachtsvorlesungsplakat 2024](file:weihnachtsvorlesung_2024.png)
