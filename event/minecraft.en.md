---
title: "Minecraft LAN Party"
slug: minecraft
lang: en

---

- ​​**When?** It starts on January 31st, 2025 at 8:00 p.m. and the event lasts the whole weekend.
- You are welcome to come a little earlier so that we can inaugurate the server together at 8:00 p.m.!
- **Who?** Anyone who wants to - regardless of whether you last played Minecraft 10 years or 10 minutes ago.
- **Where?** [Room -1.110 in the Institute of Computer Science](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110)
- Note: We only play on site so that you get the real LAN feeling.

![Poster](file:Minecraft_LAN_0125.png)

## Instructions & Rules
We will play in the computer rooms. In the SCRATCH folder there will be a script that you can use to easily install Minecraft.

When you log into the server for the first time, you have to enter a password by entering `/register <password>`. After that, you are logged in. If you are asked for one, you can log in again with `/login <password>`.

We don't want anyone to lose the fun of playing. The standards of mutual interaction are no lower in the game than outside of it. So:

1. Have fun and help your fellow players to have fun too.

2. Be nice to each other and treat each other well.

3. Players cannot hurt each other on the server (PvP is off).

Minecraft is a game with almost endless possibilities and there are many ways to have fun with it. That's why we don't want to restrict you any further, but we are available at any time if you have any questions or problems before and during the event.

## About the server
We play Vanilla Minecraft, i.e. without major modifications.
The server runs in offline mode for data protection reasons so that your personal data is not transferred to Microsoft.

**Technical details:**
We play on a Fabric server with the following mods:
- Lithium
- Async
- EasyAuth
- Carpet
- ChunkyBorder

Client-side there are also the following mods:
- Sodium
- Sodium Extra
- Bobby