---
title: "Uni-IT smarter nutzen"
slug: uni-it

relevant:
  end: 2024-11-01
  prio: 4
---

[TOC]

## Warum?
Ecampus integriert viele Dienste, macht deren Nutzung aber so unnötig kompliziert. Es ist effizienter und angenehmer direkt auf die einzelnen Dienste zuzugreifen. Auch vermeidet man dadurch Probleme, wenn Ecampus ausgefallen ist. Und es gibt auch eine Menge Dienste, die in Ecampus gar nicht integriert sind.

## Linkliste
- Stud.IP: https://www.studip.uni-goettingen.de/
- Email: https://email.stud.uni-goettingen.de/
    - Unter Android: https://k9mail.app/
    - Unter Windows/Linux/Mac: https://www.thunderbird.net/
- Raumplan: https://lageplan.uni-goettingen.de/
    - darüber kann man auch auf den Belegungsplan zugreifen
- LSG-Buchungen: https://lsg-ecampus.uni-goettingen.de
- Prüfungen/Flexnow: https://pruefung.uni-goettingen.de/
- SUB/Büchersuche: https://www.sub.uni-goettingen.de/
- Druckguthaben: https://print.student.uni-goettingen.de
- Mensa: https://www.studierendenwerk-goettingen.de/campusgastronomie/mensen/speiseplaene-der-mensen

## Welche Dienste kann nur über Ecampus nutzen?
- Rückmeldung
- Bescheinigungen (z.B. Immatrikulationsbescheinigung)
- Anträge
- Vorlesungsverzeichnis
