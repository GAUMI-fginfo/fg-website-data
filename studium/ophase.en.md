---
title: "Orientation week 2024"
slug: "ophase"

link: https://fsr.math-cs.uni-goettingen.de/en/ophase/informatik-o-phase.html
tags: event

---


You're going to have a great orientation week.
Get the most important information [here](https://fsr.math-cs.uni-goettingen.de/) on the Fachschaft's website.

![Erstiheft 2024 Deckblatt](file:Erstiheft_Deckblatt_2024_Informatik.png)

