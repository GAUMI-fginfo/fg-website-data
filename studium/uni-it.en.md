---
title: "Use Uni-IT more efficiently"
slug: uni-it

---

[TOC]

## Why?
Ecampus integrates many services but makes their use unnecessarily complicated. It is more efficient and pleasant to directly access the individual services. This also avoids problems when Ecampus is down. Additionally, there are many services that are not integrated into Ecampus at all.

## List of Links
- Stud.IP: https://www.studip.uni-goettingen.de/
- Email: https://email.stud.uni-goettingen.de/
    - On Android: https://k9mail.app/
    - On Windows/Linux/Mac: https://www.thunderbird.net/en-US/
- Room Plan: https://lageplan.uni-goettingen.de/
    - You can also access the occupancy plan through this
- LSG Bookings: https://lsg-ecampus.uni-goettingen.de
- Exams/Flexnow: https://pruefung.uni-goettingen.de/
- SUB/Book Search: https://www.sub.uni-goettingen.de/
- Print Credit: https://print.student.uni-goettingen.de
- Cafeteria: https://www.studierendenwerk-goettingen.de/campusgastronomie/mensen/speiseplaene-der-mensen

## Which services can only be used through Ecampus?
- Re-registration / Zurückmeldung
- Certificates (e.g., enrollment certificate)
- Applications
- Course catalog
