---
title: "Exam Regulations and Module Directories"
slug: pruefungsordnungen


---

Here you can find the exam regulations and module directories of the diffrent degree programms (under **Structure** => **regulations and module directory**):

- B.Sc. Applied Computer Science -- [regulations](https://www.uni-goettingen.de/de/640717.html)
- dual subject B.A. Computer Science -- regulations ([teaching profession](https://www.uni-goettingen.de/de/639315.html) / [all other profiles](https://www.uni-goettingen.de/de/639312.html))
- M.Sc. Applied Computer Science -- [regulations](https://www.uni-goettingen.de/en/40964.html) 
- M.Ed. Computer Science -- [regulations](https://www.uni-goettingen.de/de/83336.html)

Normally, the current regulations apply to you, but you may continue to study in old regulations if they change.
If you have any questions about the regulations or modules, feel free to email us at fachgruppe@informatik.uni-goettingen.de.
