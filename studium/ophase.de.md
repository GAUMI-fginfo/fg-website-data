---
title: "O-Phase 2024"
slug: "ophase"

relevant:
  end: 2024-11-01
  prio: 5
link: https://fsr.math-cs.uni-goettingen.de/de/ophase/informatik-o-phase.html
tags: event

---


Du wirst eine tolle O-Phase haben.
Die wichtigsten Informationen gibt es [hier](https://fsr.math-cs.uni-goettingen.de/) auf der Website des Fachschaftsrates Mathematik, Informatik und Data Science.

![Erstiheft 2024 Deckblatt](file:Erstiheft_Deckblatt_2024_Informatik.png)

