---
title: "Master Orientation Week"
slug: masterophase
tags: event
---

Dear students,

We are happy to invite all the new master students to the orientation week that takes place between March 27 and March 30. We are organizing some activities that offer an excellent an opportunity to learn more about your fellow students, our beautiful city and just have fun before the semester start!

## Plan

|              | Day 1 (27.03)      | Day 2 (28.03)                        | Day 3 (29.03)        | Day 4 (30.03)        |
|--------------|--------------------|--------------------------------------|----------------------|----------------------|
|11.00 - 12.30 |Info Event          | LunchBox **(12:00)**                 | -                    | -                    |
|12.30 - 14.00 | Lunch \*           | City tour **(13.00)**                | -                    | -                    |
|14.00 - 17.00 |North Campus Rallye | -                                    | -                    | -                    |
|-             |-                   | -                                    | -                    | -                    |
|-             |-                   | -                                    | -                    | -                    |
|18:00         |-                   | Board Games Night / Flunkyball       | Mario Kart Tournament| Pub Evening |

\* We are going to make some Crepes at the CS Institute, but you can also use that time to go and eat somewhere else

We also created a WhatsApp Group for a faster communication, so feel free to join: https://chat.whatsapp.com/IX1t43cWlICDTtdPPQaLFT



