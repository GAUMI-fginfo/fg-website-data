---
title: "Laptop Buying Guide for First-Semesters"
slug: laptopkaufguide
---

:::success
The Fachgruppe Computer Science advises against buying a laptop at the beginning of the first semester. Its better to wait a couple of weeks or months before deciding whether/ what kind you need.
You don't need a laptop for your studies.
:::

[TOC]

:::info
## tl;dr

- Wait a couple of weeks before making a purchase.
- A laptop is not required for studying.
- Most laptops are good enough.
- Used laptops / laptops that are a couple years old are perfectly adequate. Buying a new one is often times not necessary.
- Each of the three common operating systems can be used for your studies. 
- Avoid Chromebooks and Windows **ARM** laptops.


:::



## What is this guide for?

This guide aims to help first-semesters to decide whether / what kind of laptop they should purchase for their studies. 
The guide is intended to help you make a sensible purchasing decision.
Although this guide focuses on laptops, some of the recommendations can also be applied to tablets and desktop computers.


## Is a laptop necessary for your studies?

**No.**

You can easily complete your studies without a laptop.
Some students from higher semesters also don't have a laptop.
The [institute provides computer rooms](slug:cip) that you can visit *anytime* for studying.
All the software you need for studying is installed on these computers.

None of the courses in the first three semesters require a laptop. However, a laptop can make your studies more pleasant, especially if you dont have an apartment in Göttingen (yet) or want to take notes during lectures (although this can also be achieved using a tablet or pen and paper).

## New or used?

We usually recommend used laptops. It rarely makes sense to buy a new one. By buying a used laptop one can save a lot of money and help produce less electronic waste.
Average laptops that are only a couple years old are usually perfectly fine (see [equipment](#equipment)!)
The best way to get a good laptop is buying a used, upgradeable laptop and upgrading it. For more on this see [recommendations](#recommendations).

## Equipment

As a general rule it can be said that no special equipment is required to study computer science. An average laptop is sufficient for almost all application purposes. We generally recommend at least the following:

- $\ge$ 8 GB Memory (RAM) (16 GB if not upgradeable)
- $\ge$ 4 core CPU
    - AMD-Ryzen-CPUs are usually better.
    - Important consideration: The more powerful the CPU, the shorter the battery life. Inform yourselves about the TDP of your laptop.
    - Some older Intel-CPUs are very energy efficient.
- $\ge$ 128 GB SSD
- $>$ 6 h Battery life


## Operating Systems: Windows vs Linux vs MacOS

<!--
:::info
*Side note: What is an operating system?*
The operating system is the "main software" of your device.
Programs are explicitly developed for a specific operating system or multiple operating systems.
This means that the choice of operating system determines which programs you can run.
MacOS is made by Apple and can therefore only be used on Apple devices. Linux/Windows can be installed on any other laptop/PC.
Most laptops/PCs come with preinstalled Windows.
:::
-->

Many courses require software that was primarily developed for Linux. Since this software is open source, it usually also works on Windows and Mac. All three common operating systems can be used for studying. You don't need Linux!
However, Linux has a few advantages for studying computer science. That's why we've put together a comparison for you here. Afterwards we also give recommendations regarding the different Linux variants, which are called distributions.

Here are a few pros and cons:
- Windows:
    - Pro:
        - comes preinstalled on most devices
        - the Windows Subsystem for Linux (WSL2) and [Lifi](#lifi) provide decent Linux-compatibility.
    - Contra:
        - collects and sells your data
        - susceptible to viruses
        - a lot of ads
        - ...
- Linux:
    - Pro:
        - privacy-friendly
        - comprehensible, automatable
        - free
        - secure
        - individually customizable
        - useful in many professional computer science jobs
        - other students can support you since most of them use Linux
        - a few computer science courses have Linux as a requirement
    - Contra:
        - some software is not compatible with Linux (eg. Microsoft Office, Adobe software)
        - occasional compatibility problems with newer hardware
    - Other:
        - popular with developers
- MacOS:
    - Pro:
        - secure
        - often times very good hardware
    - Contra:
        - collects and sells your data
        - you have to install [Homebrew](https://brew.sh/) to make it useable for developing.
    - Other:
        - popular with developers
        - a lot of developer tools are written for Linux but also work under Mac

### Lifi

The intitute offers an easy to install Linux Virtual Machine which can be used under Windows as well as MacOS. This makes it possible for you to have an environment on your MacOS / Windows similar to the one in the [computer rooms](slug:cip).
*It therefore **doesn't matter** what operating system you have.* You can simply install Lifi and have a Linux environment suitable for your studies.
However, a virtual machine requires more resources and might not be the best option for laptops with weak performance.

:::success
For more information read the Lifi documentation:
https://lifi.pages.gwdg.de/lifi-dokumentation/
:::


### Linux-Distributions

:::info
Similar to Android, Linux also has different distributions.
The distribution dictates what desktop environment (how your system looks) and package management (basically app store) is used.
:::

**Ubuntu**: Beginner-friendly, is also used in our [computer rooms ](slug:cip) and for Lifi. We recommend Ubuntu if you want your environment to be as similar as possible to the one in the computer rooms.

**Mint**: Based on Ubuntu.  Has a slightly different user interface and [a few technical differences](https://linuxmint-user-guide.readthedocs.io/en/latest/snap.html) compared to Ubuntu. We recommend Mint for beginners.


**Elementary OS**: Based on Ubuntu, offers an macOS-like interface. Focuses on minimalism. We recommend this distribution for people that care about how their system looks.

**Manjaro**: Manjaro is beginner-friendly but also a popular choice for advanced users. It's based on Arch Linux and we recommend it for everyone who wants a ready-to-use system but is also interested in exploring Linux furhter in the future.

**Arch** or - even worse - **Gentoo**: If you're looking for that extra challenge. Tip: Seek assistance from students in higher semesters!

### Installing Linux

If you want to have Linux on your laptop you probably want to know how to install it.

There are a lot of very good guides on the Internet and most distributions even have an "official" guide. Most Linux distributions nowadays are easy to install. We offer a workshop for installing Linux on your laptops - the Linux Party - in the orientation week.
In case the orientation week has already passed you can also find students in higher semesters that would gladly assist you with installing Linux in our [FG-room] (slug:fgraum) or in the [computer rooms](slug:cip). Don't be afraid to ask for help, we will gladly help you!

## Recommendations

Here are a few of our recommendations:

### Avoid these laptops

Most laptops are usually good enough.
However, we recommend you try to avoid laptops with these properties/by these manufacturers:
- Chromebooks
    - Chromebooks are designed for web browser-usage and are therefore restrictive with their support for software. There is a way to run Linux-software on a Chromebook but we have no experience with that. If you plan on using a Chromebook for your studies, feel free to reach out to us so we can expand this paragraph!

- Windows on **ARM** Laptops
    - We are specifically referring to Windows laptops with **ARM** CPU (instead of Intel or AMD).
    - Laptops with ARM CPU often times do not explicitly market themselves as such so don't forget to take a look at the technical specifications.
    - There's nothing wrong with ARM CPUs. The **problem** is that Windows laptops with ARM chips often times don't allow their users to install alternative operating systems. Support for ARM Software under Windows is also still lacking - although Microsoft is working on that.
- https://dontbuydell.com/
    - Caution: This article contains personal views and experiences that we are simply sharing without commenting on them. Not everyone's experiences are the same. Please form your own opinions!
- Gaming laptops or laptops with a dedicated graphics card in case you're looking for long battery life.

### Patience with your purchasing decision

You really don't need a laptop on your first day of your studies.
Wait a few weeks in case you really want to buy a laptop and consider talking to students in higher semesters in order to make a well-thought-out purchasing decision.

### Specific models

You want a laptop and are looking for concrete starting points? Here are some of our recommendations:

:::warning
Please enjoy these recommendations **with a lot of caution**!
:::

- used Thinkpads on Ebay, Backmarket, Refurbed or similar websites
    - Pro:
        - cheap
        - usually compatible with Linux
        - Suitable models are e.g. T-Series (T4XX, T14 Gen X, T15 Gen X), L-Series (L4XX, L14 Gen X, L5XX, L15 Gen X), X1 Carbon Gen X (Caution: Not all of them are upgradeable!)
    - Contra:
        - Caution: quality and compatibility can differ greatly between thinkpads!
- Tuxedo Computers
    - Pro:
        - moderately priced
        - excellent Linux compatibility
        - good customer service
    - Contra
        - limited selection
        - Not always the highest quality (e.g. printed-on keyboard layout, loose rubber feet)
- Framework 13 (possibly soon also Framework 16)
    - Pro:
        - repairable
        - upgradeable
        - good customer service
        - good linux compatibility
    - Contra:
        - expensive




