---
title: "Laptop Kauf Guide für Erstis"
slug: laptopkaufguide

relevant:
  end: 2023-11-01
  prio: 4
---

:::success
Die Fachgruppe rät vom Kaufen eines Laptops zu Beginn des ersten Semesters ab.
Lieber ein paar Wochen oder Monate warten und dann entscheiden, ob man einen/welchen man braucht.
Das Studium kann auch ohne Laptop absolviert werden.
:::

[TOC]

:::info
## tl;dr

- Wartet mit einer Kaufentscheidung einige Wochen ab.
- Für's Studium wird kein Laptop benötigt.
- Die allermeisten Laptops sind gut genug.
- Gebrauchte Laptops / Laptops der letzten Jahre sind vollkommen ausreichend. Neukauf ist häufig nicht sinnvoll.
- Jedes der drei gängigen Betriebssysteme ist für's Studium nutzbar.
- Finger Weg von Chromebooks und Windows **ARM** Laptops.

:::



## Wofür ist dieser Guide?

Dieser Guide soll Studis, die gerade mit ihrem Studium anfangen (Erstis) helfen zu entscheiden **ob** und ggf. **was für einen** Laptop ihr euch für euer Studium anschaffen solltet. Dieser Guide soll euch helfen eine sinnvolle Kaufentscheidung zu treffen.

Auch wenn wir in diesem Guide einen Fokus auf Laptops legen, lassen sich die Empfehlungen auch teilweise auf Tablets und Desktop-Computer übertragen.

## Wird ein Laptop für's Studium benötigt?

**Nein.**

Das Studium lässt sich problemlos auch ohne Laptop bewältigen.
Manche Studis aus höherem Semester besitzen auch keinen Laptop.
Das [Institut stellt extra Rechnerräume bereit](slug:cip), wo man *jederzeit* hingehen und arbeiten kann.
Alles an Software die man für's Studium benötigt ist auf diesen Rechnern installiert.

In den ersten drei Semestern wird in keiner Veranstaltung ein Laptop vorrausgesetzt.

Ein Laptop kann das Studium aber angenehmer machen, insbesondere wenn man (noch) keine Wohnung in Göttingen hat oder wenn man während einer Vorlesung Notizen machen möchte (hierfür bietet sich aber auch ein Tablet oder Stift und Papier an).

## Neu oder Gebraucht?

Wir empfehlen tendenziell gebraucht, Neukauf ist selten sinnvoll.
Durch einen Gebrauchtkauf spart man viel Geld und trägt dazu bei, weniger Elektromüll zu produzieren.
Außerdem sind durchschnittlich ausgestattete Laptops der letzten paar Jahre leistungstechnisch meistens vollkommen ausreichend (siehe [Ausstattung](#ausstattung))!

Die beste Möglichkeit um einen guten, gut ausgestatteten Laptop zu erhalten ist einen gebrauchten erweiterbaren Laptop zu kaufen und ihn aufzurüsten. Mehr dazu findet ihr unter [Empfehlungen](#empfehlungen).

## Ausstattung
Grundsätzlich lässt sich sagen, dass für ein Informatikstudium keine besondere Ausstattung benötigt wird. Ein Durchschnittslaptop ist für nahezu alle Anwendungszwecke ausreichend.

Es geht auch mit weniger, und mehr kann auch für viele Dinge angenehmer sein. Wir würden für einen Laptop trotzdem mindestens Folgendes empfehlen:

- $\ge$ 8 GB RAM/Arbeitsspeicher (16 GB falls nicht erweiterbar)
- $\ge$ 4 Kerne CPU
    - AMD-Ryzen-CPUs sind tendenziell besser
    - Wichtige Abwägung: Je leistungsfähiger die CPU, desto kürzer die Akkulaufzeit. Informiert euch dazu über die TDP von Laptop+CPU
    - ältere Intel-CPUs sind teilweise sehr stromsparend (lange Akkulaufzeit)
- $\ge$ 128 GB SSD
- $>$ 6 h Akkulaufzeit


## Betriebssystem: Windows vs Linux vs MacOS

<!--
:::info
*Exkurs: Was ist ein Betriebssystem?*
Das Betriebssystem ist die "Hauptsoftware" auf eurem Gerät.
Programme werden explizit für ein bestimmtes oder mehrere Betriebssysteme entwickelt.
Die Wahl des Betriebssystems entscheidet also welche Programme man ausführen kann.

MacOS ist von Apple, kann^[de facto] nur auf Applegeräte verwendet werden, und ist dort auch vorinstalliert.
Auf allen sonstigen Laptops/Rechnern kann man Windows oder Linux installieren.
Die aller meisten Laptops/Rechner kommen mit Windows vorinstalliert.
:::
-->

> Ach ja. Die alte Debatte schon wieder...

Im Studium wird in vielen Veranstaltungen Software benötigt die vorrangig für Linux entwickelt wurde. Da diese Software Open Source ist, funktioniert sie aber meistens auch unter Windows und Mac. Alle drei gängigen Betriebssysteme sind also für das Studium nutzbar. Ihr benötigt kein Linux!

Linux hat aber Vorteile im Informatik Studium. Deshalb haben wir euch hier einmal einen Vergleich zusammengestellt. Im Anschluss geben wir auch noch Empfehlungen bzgl. der verschiedenen Linux-Varianten, die Distributionen genannt werden.

Hier ein paar Vor- und Nachteile:
- Windows:
    - Pro:
        - kommt vorinstalliert (viel mehr ist uns nicht eingefallen)
        - hat tatsächlich mit dem Windows Subsystem for Linux (WSL2) oder [Lifi](#lifi) eine gute Linux-Kompatibilität
    - Contra:
        - Trackt dein Verhalten
        - virenanfällig
        - Werbung
        - viele...
- Linux:
    - Pro:
        - datenschutzfreundlich
        - verstehbar, automatisierbar
        - kostenlos
        - sicher
        - nahezu beliebig individuell anpassbar, viel Auswahl
        - ist für die berufliche Weiterentwicklung hilfreich
        - man erhält im Studium unterstützung damit
        - Einige Veranstaltungen im Informatik Studium gehen von Linux als Betriebssystem aus.
    - Contra:
        - Manche Software funktioniert nicht (Microsoft Office, Adobe Produkte)
        - bei neuer Hardware manchmal Probleme mit der Kompatibilität
    - Sonstiges:
        - Häufig bei Entwicklern beliebt.
- MacOS:
    - Pro:
        - sicher
        - Macs haben häufig sehr gute Hardware.
    - Contra:
        - Trackt dein Verhalten.
        - Man muss [Homebrew](https://brew.sh/) installieren um es zum Entwickeln nutzbar zu machen.
    - Sonstiges:
        - Häufig bei Entwicklern beliebt.
        - Viele Entwicklertools sind vorrangig für Linux geschrieben, laufen aber meistens auch unter Mac.


### Lifi

Das Institut stellt für Studis (insbesondere Erstis) eine einfach aufzusetzende Linux Virtuelle Maschine bereit, welche unter Windows und MacOS verwendet werden kann.

Diese ermöglicht es euch auf eurem Windows Laptop oder Mac eine ähnliche Umgebung wie auf den [Rechnerräumen im Institut](slug:cip) zu bekommen.

*Entsprechend ist es **egal** welches Betriebssystem ihr habt.* Ihr könnt einfach Lifi installieren und habt eine fertige Umgebung in der ihr eure Uni Sachen problemlos in einem Linux machen könnt.

Eine Virtuelle Maschine benötigt aber natürlich auch mehr Ressourcen und läuft deshalb auf älteren oder leistungsschwachen Laptops entsprechend vielleicht langsamer.

:::success
Weitere Informationen gibt es in der Lifi Dokumentation:
https://lifi.pages.gwdg.de/lifi-dokumentation/
:::


### Linux-Distributionen

:::info
Ähnlich zu Android gibt es auch bei Linux unterschiedliche Distributionen.
Die Distribution gibt vor, welche Desktopumgebung verwendet wird (also wie es aussieht) und welche Paketverwaltung (quasi App Store) verwendet wird.
:::

**Ubuntu**: Einstiegsfreundlich, kann man ohne Probleme einfach so verwenden, wird auch in [unseren Rechnerräumen](slug:cip) und für Lifi verwendet. Wir empfehlen Ubuntu, wenn euch besonders wichtig ist, dass die Umgebung möglichst nah an den Rechnerräumen liegt.

**Mint**: Basiert auf Ubuntu. Hat eine leicht andere Arbeitsoberfläche und [trifft bewusst ganz wenige technische Entscheidungen anders](https://linuxmint-user-guide.readthedocs.io/en/latest/snap.html). Wir empfehlen grundsätzlich Mint als Einstieg.

**Elementary OS**: Basiert auf Ubuntu, bringt aber eine MacOS-artige Oberfläche mit. Setzt außerdem einen Fokus auf Minimalismus. Empfehlen wir für alle, denen besonders wichtig ist, wie ihr System aussieht.

**Manjaro**: Manjaro ist zwar einstiegsfreundlich, bietet aber auch sehr viele Möglichkeiten für Fortgeschritteneres. Es basiert auf Arch Linux und wir empfehlen es für alle, die direkt ein nutzbares System haben wollen, aber mittelfristig tiefer in Linux einsteigen wollen.

**Arch** oder - noch schlimmer - **Gentoo**: Wenn ihr die besondere Herausforderung sucht. Tipp: Macht das nicht alleine, sondern holt euch Unterstützung von älteren Semestern!

### Linux installieren

Falls ihr Linux auf eurem Laptop haben wollt, wollt ihr sicher wissen wie das geht.

Es gibt sehr viele gute Anleitungen im Internet wie man Linux installiert und fast jede Linux-Distro hat auch eine eigene Anleitung.
Die meisten Linux-Distros sind mittlerweile so nutzerfreundlich bei der Installation, dass es einfacher ist Linux auf einem Rechner zu installieren als Windows. (Aber Windows kommt halt häufig schon vorinstalliert.)

In der O-Phase bieten wir immer einen Workshop zum Installieren von Linux auf euren Laptops an. Die so genannte Linux-Party.

Falls die O-Phase schon vorbei ist, findet ihr häufig Studis höherer Semester in [unserem Fachgruppenraum](slug:fgraum) oder in einem [Rechnerraum](slug:cip) die euch meistens sehr gerne beim Linux-Installieren helfen oder jemanden kennen der euch helfen kann.
Habt wirklich keinen Scham dort jemanden anzusprechen. Wir helfen euch gerne.


## Empfehlungen

Hier einige spezifische Empfehlungen von uns:

### Finger Weg von diesen Laptops

Grundsätzlich sind die meisten Laptops gut genug.

Von den folgenden Herstellern/Modellen/Eigenschaften empfehlen wir dennoch die Finger wegzulassen:
- Chromebooks
    - Chromebooks sind nur sehr eingeschränkt nutzbar, auch wenn sie intern auf Linux basieren. Sie sind größtenteils auf Webbrowser-Nutzung ausgelegt. Zwar gibt es inzwischen die Möglichkeit Linux-Software darüber zu nutzen, aber wir haben keine Erfahrung damit, wie gut das funktioniert und ob das für's Studium gut funktioniert. Wenn ihr so einen Laptop ins Studium mitbringt, meldet euch bei uns, damit wir diesen Absatz erweitern können. Bis dahin können wir vorerst nur davon abraten!
- Windows on **ARM** Laptops
    - Wir meinen hier explizit Windows Laptops mit einer **ARM** CPU (statt Intel oder AMD).
    - Laptops mit ARM CPU bewerben häufig nicht, dass sie eine ARM CPU haben, deshalb genau in die technischen Details gucken vor dem Kaufen!
    - An ARM CPUs an sich ist nichts auszusetzen. Das **Problem** ist mehr das Windows Laptops mit ARM Chip häufig nicht zulassen alternative Betriebssysteme zu installieren und der Support für ARM entwickelte Software auf Windows immer noch sehr schlecht ist (auch wenn Microsoft da aktiv dran arbeitet).
- https://dontbuydell.com/
    - Achtung: Da ist eine Menge Meinung und Hörensagen drin, die wir hier unkommentiert weiter geben. Nicht jeder macht die gleiche Erfahrung. Bildet euch selbst eine Meinung!
- Gaminglaptops oder eine dedizierte GPU, falls ihr eine hohe Akkulaufzeit anzielt

### Geduld bei der Kaufentscheidung

Ihr braucht wirklich keinen Laptop zum ersten Tag des Studiums.

Falls ihr euch unbedingt einen Laptop kaufen wollt, wartet ein paar Wochen und unterhaltet euch mit Studis aus höheren Semestern.
So könnt ihr besser einschätzen was ihr wirklich braucht und was ihr nicht braucht und könnt eine sinnvollere Kaufentscheidung treffen.

### Konkrete Modelle

Ihr wollt trotzdem einen Laptop und ein paar konkrete Ansatzpunkte? Hier sind einige persönliche Empfehlungen von aktiven Mitgliedern der Fachgruppe:

:::warning
Genießt alle diese Empfehlungen mit **sehr viel Vorsicht**!
:::

- gebrauchte Thinkpads auf Ebay, refurbed, Back Market, oder ähnlichen Seiten
    - Pro:
        - günstig
        - bei den meisten Laptops Linux-Kompatibilität
        - Passable Modelle sind z.B. T-Serie (T4XX, T14 Gen X, T15 Gen X), L-Serie (L4XX, L14 Gen X, L5XX, L15 Gen X), X1 Carbon Gen X (einzelne; Achtung: RAM nicht erweiterbar)
    - Contra:
        - Achtung: Die Linux-Kompatibilität und grundsätzliche Qualität fällt bei Thinkpads sehr unterschiedlich aus!
- Tuxedo Computers
    - Pro:
        - Moderater Preis
        - Exzellente Linuxkompatiblität
        - fairer Umgang mit den Kunden
    - Contra:
        - Eingeschränkte Auswahl
        - Moderater Preis
        - Nicht immer die höchste Qualität (z.B. aufgedruckte Tastaturlayout, Gummifüße die sich lösen)
- Framework 13 (ggf. bald auch Framework 16)
    - Pro:
        - reparierbar
        - erweiterbar
        - fairer Umgang mit Kunden
        - gute Kompatibilität mit Linux
    - Contra:
        - teuer




