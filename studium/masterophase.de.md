---
title: "Master O-Phase"
slug: masterophase
relevant:
  end: 2023-03-30
  prio: 2
tags: event
---

Liebe Studierende,

Wir freuen uns, alle neuen Masterstudierende zur O-Phase begrüßen, die vom 27. bis 30. März stattfindet. Wir organisieren Aktivitäten, die eine sehr gute Möglichkeit bieten, deine Mitstudierende und die schöne Stadt kennenzulernen und einfach vor Semesterstart Spaß zu haben!

## Plan

|              | Tag 1 (27.03)      | Tag 2 (28.03)                        | Tag 3 (29.03)        | Tag 4 (30.03)        |
|--------------|--------------------|--------------------------------------|----------------------|----------------------|
|11.00 - 12.30 |Info Event          | LunchBox **(12:00)**                 | -                    | -                    |
|12.30 - 14.00 | Mittagessen \*           | Stadtrundgang **(13.00)**                | -                    | -                    |
|14.00 - 17.00 |Nordcampusrally | -                                    | -                    | -                    |
|-             |-                   | -                                    | -                    | -                    |
|-             |-                   | -                                    | -                    | -                    |
|18:00         |-                   | Spieleabend / Flunkyball       | Mario Kart Turnier| Pubabend |

\* Wir werden am IfI Crepes machen, aber du kannst auch diese Zeit nutzen, um wo anders etwas zu essen

Wir haben auch eine WhatsApp Gruppe für schnellere Kommunikation erstellt, du kannst über folgenden Link gerne beitreten: https://chat.whatsapp.com/IX1t43cWlICDTtdPPQaLFT



