---
title: "Prüfungs&shy;ordnungen & Modul&shy;verzeichnisse"
slug: pruefungsordnungen

relevant:
  end: 2022-11-01
  prio: 3

---

Hier findest du die Prüfungsordnungen und Modulverzeichnisse der verschiedenen Studiengänge (unter **Studienaufbau** => **Aktuelle und Ältere Fassungen**):
- B.Sc. Angewandte Informatik -- [Ordnungen](https://www.uni-goettingen.de/de/640717.html)
- 2-Fach B.A. Informatik -- Ordnungen ([Profil Lehramt](https://www.uni-goettingen.de/de/639315.html) / [alle anderen Profile](https://www.uni-goettingen.de/de/639312.html))
- M.Sc. Angewandte Informatik -- [Ordnungen](https://www.uni-goettingen.de/de/ordnungen/40964.html) 
- M.Ed. Informatik -- [Ordnungen](https://www.uni-goettingen.de/de/83336.html)

Normalerweise gelten die aktuellen Ordnungen für euch, ihr könnt aber ggf. in alten Ordnungen weiterstudieren, wenn sie sich ändern.
Wenn ihr Fragen zu Ordnungen oder Modulen habt, schreibt uns gerne eine E-Mail an fachgruppe@informatik.uni-goettingen.de.
