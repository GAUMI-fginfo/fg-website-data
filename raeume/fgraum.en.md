---
title: "Fachgruppe's room"
slug: fgraum

tags: fachgruppe
---

The Fachgruppe's room is explicitly thought of as a room for all students of the Fachgruppe(YOU). You can hang out, socialize, or study there.

The room has a sofa, chairs and tables. Here you can chill or talk with your fellow students. 
The Fachgruppe's room is in [the Institute of Computer Science on the groundfloor (-1)](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.108). 

![The Fachgruppe's room](file:fgraum_2020.jpg "The Fachgruppe's room in 2020.")

[TOC]


## :game_die: Game Cabinet {#spieleschrank}

The game cabinet contains many diffrent card and board games :jigsaw:. On [weekly game nights] we play these games. 

The cabinet is usualy locked. :lock:. If you want to access the cabinet, just ask someone from a higher semester e.g. at the [CIP-Pool](slug:cip).

## :joystick: Retro Gaming Station {#retrogamingstation}

While there are only analog games in the [Game Closet](#spieleschrank), there are also digital games in the Fachgruppe's room in the form of the *Retro Gaming Station*.
Here **you can play good old games with up to three other people**.

The Retro Gaming Station consists of a Raspberry Pi keyboard :keyboard:, a plasma TV :tv: and four controllers :video_game:.

:::warning
:electric_plug: The plasma TV consumes a lot of power. Please turn it off after each use.
:::

There are several emulators and games installed on the Retro Gaming Station (Gamecube, N64, SNES, etc...).

Use the Retro Gaming Station to make new friends in the student body, to take a break between lectures, to counteract exam stress, or to avoid studying for your exam next week :wink:.
