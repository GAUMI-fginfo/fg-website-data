---
title: "Fachgruppenraum"
slug: fgraum

tags: fachgruppe
---

Der Fachgruppenraum ist explizit als Aufenthaltsraum zum Sozialisieren und Studieren für alle Studis der Fachgruppe Informatik (also du!) gedacht.

Im Fachgruppenraum steht ein Sofa, Stühle und Tische, an denen du chillen oder dich mit deinen Kommoliton:innen :tipping_hand_woman: austauschen kannst.
Der Fachgruppenraum befindet sich in der [Etage -1 im Raum -1.108 des Institutes für Informatik](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.108).

![Der Fachgruppenraum](file:fgraum_2020.jpg "Der Fachgruppenraum im Jahre 2020.")

[TOC]


## :game_die: Spieleschrank {#spieleschrank}

Im Spieleschrank befinden sich viele verschiedene Brett- und Kartenspiele :jigsaw:, welche bei [den wöchentlichen Spieleabenden](slug:spieleabend) verwendet werden.

Der Schrank ist aber normalerweise abgeschlossen :lock:. Fragt ruhig jemanden aus einem höheren Semester im [CIP-Pool](slug:cip), falls ihr an den Schrank wollt, viele Personen können euch da weiterhelfen :unlock:.


## :joystick: Retro Gaming Station {#retrogamingstation}

Während es im [Spieleschrank](#spieleschrank) nur analoge Spiele gibt, gibt es im Fachgruppenraum auch digitale Spiele in Form der *Retro Gaming Station*.
Hier kannst **du mit bis zu drei weiteren Personen** gute alte Spiele spielen.

Die Retro Gaming Station besteht aus einer Raspberry Pi Tastatur :keyboard:, einem Plasma Fernseher :tv: und vier Controllern :video_game:.

:::warning
:electric_plug: Der Plasma Fernseher verbraucht sehr viel Strom. Bitte nach jeder Nutzung wieder ausschalten.
:::

Auf der Retro Gaming Station sind verschiedene Emulatoren und Spiele installiert (Gamecube, N64, SNES, etc...).

Nutze die Retro Gaming Station um neue Freunde in der Studierendenschaft zu finden, eine Pause zwischen Vorlesungen zu machen, dem Prüfungsstress entgegen zu wirken, oder das Lernen für deine Klausur nächste Woche zu umgehen :wink:.

