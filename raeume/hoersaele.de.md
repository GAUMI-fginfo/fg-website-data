---
title: "Hörsäle"
slug: hoersaele

---

Das Institut für Informatik selbst hat keinen eigenen Hörsaal, weswegen wir die Hörsäle der Geowissenschaften benutzen dürfen. Diese befinden sich direkt im Nachbargebäude und bieten aussreichend Platz für unsere Veranstaltungen. Hier findet ihr die Links zu den einzelnen Hörsälen auf dem Lageplan:

- [MN08](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.104)
- [MN09](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.102)
- [MN14](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.112)
- [MN15](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.110)


![Der Hörsaal MN08.](file:hoersaal_mn08.JPG) 
## Eingänge

Es gibt mehrere Möglichkeiten, zu den Hörsälen zu gelangen. Zum einen können [die Glastüren](https://lageplan.uni-goettingen.de/?ident=2410_1_EG_0.169) benutzt werden, die vom Haupteingang des Instituts gut erreichbar sind oder es wird [der Haupteingang der Geo](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.142) benutzt, der auf der anderen Seite des Gebäudes liegt. Sollte dieser Eingang gewählt werden, so sind die Hörsäle erreichbar, indem dem Flur nach der Tür geradeaus gefolgt wird.
