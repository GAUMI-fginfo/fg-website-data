---
title: "Küche"
slug: kueche

---

The coffee kitchen is located on [floor -1 in the Institute of Computer Science in the hallway to the right](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.113). The kitchen is for everyone. To make it work and to make us all comfortable with using it, we need some common rules, which can also be found on the signs in the kitchen.
The Fachgruppe Computer Science takes care that the kitchen is equipped with the most important consumables. If something is empty, just let us know: fachgruppe@informatik.uni-goettingen.de or ask someone in the Fachgruppe's room.

![A photo of the kitchen in tidy condition](file:kueche2022.jpg "The kitchen")

[TOC]

## Consumables
The department of computer science takes care of supplies
* dishwasher tabs
* washing-up liquid
* dishcloths
* sharpie

## Use of the kitchen
:::success
Whoever uses the kitchen should also help to keep it clean.
:::

### The dishwasher

_Have you used dishes?_ Just put them in the dishwasher and don't leave them in the Fachgruppe's room or on the sink.

_Is the dishwasher full and clean?_ No problem, please clean it out, you have put things in there before. 
We, as students of computer science, are all responsible for the maintenance of the kitchen.

_Is the dishwasher full and dirty?_ No problem, the dishwasher tabs are in the drawer above the refrigerator, the preset standard program works wonderfully. To start, press the power button and then push the door shut until it clicks into place and the rinse program will begin. 

### The refrigerator

The refrigerator can be used for food and beverages by all students. 

However, it is important that you label __your things__ (name/pseudonym and opening date if you are storing it there for a longer period of time). Everything that is not labeled is considered common property and may be used freely. 

You will find a __permanent marker__ for labeling in the drawer above the refrigerator. 

### Microwave

There are two __microwave lids__. Please use these (or another form of cover) when you heat something up in the microwave. If a microwave hood is dirty, it should be put in the dishwasher or washed briefly by hand.
If you find a dirty microwave, unfortunately someone else has not followed the rules. Even if you didn't cause it, it would still be nice to clean the microwave.

### Water heater

:::warning
Please do not use at this time. Water boilers can heat a lot of volume at a time, but require a relatively large amount of energy.
:::

The unit above the sink is a water boiler with an integrated faucet. Here you can tap both cold tap water and boiled water (if any has been boiled). 
There are also __pictured instructions above__ that explain how the whole thing works. 

### Electric kettle

If you are too intimidated by the water boiler or want to make it quick, there is also a conventional kettle on the sideboard that you can use. Please note that you should only fill it up to the "max" mark to avoid flooding. 

### Coffee machine

You are welcome to use the coffee machine to make coffee. 
It is usually worth asking around if others would like to drink coffee and then making a large pot. The coffee maker can also keep the coffee warm if it is not turned off. 

Please clean the filter holder and the pot before you leave. 

How to make coffee in the filter machine: 

* If on, then turn off the coffee maker to turn off the heating elements.

* Fold a coffee filter on the perforated sides, unfold it and put it in the filter holder.

* Pour the desired amount of water + 1 cup extra (lost in the brewing process) into the water tank. Feel free to simply use the jug and measuring scale on the tank for this. 

* Fill the coffee powder into the filter. Roughly you can calculate 1 spoon of powder per cup of water. 

* Place the filter holder in position over the pot, turn the coffee machine on at the red toggle switch on the right and wait for the water to run through. The machine will then make puffing sounds when it is ready. 


### Tea, coffee, etc.

Both coffee and tea in the cupboards are there for everyone. 
Everyone is allowed to help themselves here. 

The important thing to remember here is that the supplies are from students for students.

This means that if you are using them, please restock them yourself at some point to keep this system working.

Exceptions are of course labeled things, but please make sure not to overfill the cupboards with private property. 

---

## FAQ
- __I would like to help to keep the kitchen in a clean and tidy condition, what can I do?__ : Not letting your own things go bad in the fridge, cleaning out clean dishwashers, washing large items by hand, and cleaning the microwave are the best ways to help.
- __Help, some of my food has disappeared from the fridge.__ : Every now and then a person cleans the fridge and sorts out unlabeled, bad and/or expired things. This is probably what happened and your stuff fit these criteria.
- __ There is a sign in the kitchen that says to clean my stuff out of the fridge. Why?__ : It's probably orientation week soon or some other event where cooking is going on. The groceries for that take up quite a bit of space, which is why the FSR and Fachgruppe rely on the space.
- __I'd like to request something for the kitchen__ : Then write your suggestion to the Fachgruppe. We will then see if your idea can be implemented.
- __I have an idea for this mini-FAQ__ : Then write it to the Fachgruppe.
