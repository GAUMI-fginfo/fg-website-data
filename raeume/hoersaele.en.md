---
title: "Lecture halls"
slug: hoersaele

---

The Institute of Computer Science itself does not have its own lecture hall, which is why we are using the lecture halls of the Geosciences. These are located directly in the neighboring building and offer enough space for our events. Here you can find the links to the individual lecture halls on the site plan:

- [MN08](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.104)
- [MN09](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.102)
- [MN14](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.112)
- [MN15](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.110)


![Lecture Hall MN08.](file:hoersaal_mn08.JPG) 
## Entrances

There are several ways to get to the lecture halls. One way is to use the [glass doors](https://lageplan.uni-goettingen.de/?ident=2410_1_EG_0.169), that are easily accessible from the main entrance of the institute, alternatively the [main entrance of the Geosciences building](https://lageplan.uni-goettingen.de/?ident=2409_1_EG_0.142) can be used, which is located on the other side of the building. If this entrance is chosen, the lecture halls can be reached by following the corridor straight ahead after the door.
