---
title: "Computer rooms"
slug: cip

---

The CIP pools are spread across several buildings, [three of them on floor -1 in the Institute of Computer Science](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.111) and [one in the Provisorium](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.103). In those rooms there are computers availible for free use. Study groups also meet here and students exchange information about modules or their studies in general. Students from higher semesters can be found here, who are usually happy to help if you have problems.

![CIP-Pool -1.101](file:cip1101_leer.JPG)

[TOC]

## The computers

You can log on to the computers with your student account. The first time you log in, an account will be created. This can take a few minutes. Each account is allocated 10GB of storage. The computers run Ubuntu (Linux) and have everything needed for the lectures installed. Bugs and suggestions can be reported at https://gitlab.gwdg.de/cip/pool-bugs/-/issues. 

## Follow-Me Printer
In room -1.110 you will find one of the Follow-Me printers, as they are distributed all over the university. Print jobs that you have uploaded to the web portal (https://print.student.uni-goettingen.de) can be printed there using your student ID card.
The special feature of the Institute of Computer Science is that you can also access the Follow-Me printer in the CIP pool directly from the computers there.

## Further equipment

All CIP-Pool rooms are equipped with at least one projector (primarily for events) and a whiteboard or SmartBoard. These can also be used by students as long as no one is disturbed by this use.

- In [Room -1.110](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110), in the first cabinet to the left of the entrance, there is a stapler and binding set (for binding books, lecture notes, papers, etc.).
- In [room -1.111](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.111) is a shelf with various books. 
- [Room -1.101](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.101) and [the CIP pool in the Provisorium](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.103) are also seminar rooms and are equipped accordingly.

Rooms [-1.110](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110), [-1.111](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.111) and [-1.101](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.101) are each equipped with an air conditioning system and an air exchanger. The latter automatically exchanges indoor air with fresh outdoor air and brings it to room temperature. Particularly in winter and midsummer, prolonged manual ventilation is therefore not advisable.

![CIP-Pool 1.110](file:cip1110_leer.JPG)

