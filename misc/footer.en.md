--- 
title: "Footer"
status: hidden
---

:::::::::::::: {.columns}
::: {.column style="--col-width: 20%;"}

[![Logo](file:banner-logo.png "Home"){style="background-color:#fff;border-radius:0.5em;padding:0.2em;"}](slug:index)

:::
::: {.column style="--col-width: 30%;"}
:::
::: {.column style="--col-width: 50%;"}

[:arrow_up: Back to top.](#top)

:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column style="--col-width: 50%;"}

## Legal

- [Privacy policy](slug:datenschutz)
- [Imprint](slug:impressum)

## Contact

| Fachgruppe Informatik Georg-August-Universität Göttingen
| Goldschmidtstr. 7
| 37077 Göttingen 

:::
::: {.column style="--col-width: 50%;"}

## Links

- [About us](slug:fachgruppe)
- [Mastodon](slug:mastodon)
- [Campus map](slug:lageplan)
- [Fachschaft's website](https://fsr.math-cs.uni-goettingen.de/en/)
- [AStA of the University of Göttingen](https://asta.uni-goettingen.de/en)
- [Institute of Computer Science](https://uni-goettingen.de/en/619480.html)
- [Studentenwerk](https://www.studentenwerk-goettingen.de/en)
- [University of Göttingen](https://www.uni-goettingen.de/en)
<!--- [Fachgruppe DataScience](tba)-->

:::
::::::::::::::

:email:&nbsp;fachgruppe{at}informatik.uni-goettingen.de

© Fachgruppe Informatik Uni Göttingen, 2022.

