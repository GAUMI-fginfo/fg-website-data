---
title: "Events"
#color: "hsl(345, 86%, 56%)"
slug: "tag-event"

---

The Fachgruppe regularly organizes events with a wide variety of content, so that there is something for everyone. 
During the winter semester some of the highlights are the [orientation week](slug:ophase) and the [christmas lecture](slug:weihnachtsvorlesung), and during the summer semester the [student computer science days](slug:sit).


