---
title: "Räumlichkeiten"
#color: "hsl(294, 62%, 39%)"
#color: "oklch(79.96% 0.162 288)"
color: "#b9abff"
slug: "tag-raeume"

after:
  - type: "iframe"
    url: "https://wwwuser.gwdg.de/~fginfo/i/"
    title:
      de: "Virtuelle Institutstour (2020)"
      en: "Virtual Institutstour (2020) (German)"

---

<!--Die Räume des Institus sind unergründlich... *oh* hier sind sie ja aufgelistet.
Zumindest alle Räumlichkeiten die für dein Studium relevant sind.-->

Hier sind alle für dein Studium relevante Räumlichkeiten.

