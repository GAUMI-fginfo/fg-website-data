---
title: "Get Involved!"
#color: "hsl(157, 77%, 37%)"
slug: "tag-engage"

---

Without your support many of or events wouldn't exist.
Here are some suggestions for how you can get involved, from helping out in the orientation week to beeing an active member in the Fachgruppe, and more.
