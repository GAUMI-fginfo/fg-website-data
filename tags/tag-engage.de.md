---
title: "Engagier Dich!"
#color: "hsl(157, 77%, 37%)"
#color: "oklch(79.96% 0.162 144)"
color: "#77d877"
slug: "tag-engage"

---

Ohne eure Unterstützung gäbe es viele Angebote im und ums Studium herum nicht.
Hier findet ihr deshalb ein paar Anregungen, wie ihr euch einbringen könnt, von der Mithilfe in der OPhase bis zum aktiven Mitmachen in der Fachgruppenarbeit, aber auch darüberhinaus.
