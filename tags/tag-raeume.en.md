---
title: "Room Tour"
#color: "hsl(294, 62%, 39%)"
slug: "tag-raeume"

after:
  - type: "iframe"
    url: "https://wwwuser.gwdg.de/~fginfo/i/"
    title:
      de: "Virtuelle Institutstour (2020)"
      en: "virtual tour of the institute (2020) (German)"

---

Here you can find all rooms, that might be relevant for your studies.

