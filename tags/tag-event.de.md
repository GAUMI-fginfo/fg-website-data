---
title: "Veranstaltungen"
#color: "hsl(345, 86%, 56%)"
#color: "oklch(79.96% 0.162 0)"
color: "#ff8db9"
slug: "tag-event"

---

Die Fachgruppe organisiert regelmäßig Veranstaltungen mit den unterschiedlichsten Inhalten, so dass für alle etwas dabei ist. 
Highlight im Wintersemester ist neben der [O-Phase](slug:ophase) die [Weihnachtsvorlesung](slug:weihnachtsvorlesung) und im Sommersemester die [studentischen Informatiktage](slug:sit).


