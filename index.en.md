---
title: Home
template: "index.html"
after:
  - type: "relevant"
    title:
      de: "Aktuelles"
      en: "News"
    id: "relevant"
  - type: "iframe"
    url: "https://cloud.asta.uni-goettingen.de/apps/calendar/embed/g8psWpbfafNM6Rpo-K6EpnXeMFdRZacDj"
    title:
      de: "Kalender"
      en: "Calendar"
    id: "calendar"
  - type: "tag"
    tag: "engage"
    num: 5
    id: "engage"
  - type: "news"
    title:
      de: "Zuletzt Veränderte Seiten"
      en: "Last changed pages"
    id: "last_changed"
    num: 5

---

Welcome to the website of the Fachgruppe Computer Science. Here you can find general information about us, our current events, your study and more.
