---
title: "Über uns"
slug: fachgruppe

---

Wir sind eine Gruppe von engagierten Studis aus der Informatik. Wir organisieren jedes Semester verschiedene Veranstaltungen, wie die O-Phase, Weihnachtsvorlesungen oder auch die [Cookie-Talks](slug:cookie-talks). Neben der Vernetzung ist die Studienqualität in der Informatik ein wichtiges Anliegen. Dabei verlassen wir uns nicht nur auf unsere eigene Erfahrung, sondern bekommen auch immer wieder Feedback von euch. Wenn du eine Anregung hast, dann schreib uns gerne eine Mail oder komm mal zu den Fachgruppentreffen vorbei.
Einige von uns sitzen zudem auch in den Gremien der Universität, z.B. der Studienkommission, die sich gezielt mit Studienqualität beschäftigen oder in der Prüfungskommission, die sich u.A. auch mit Härtefallanträgen.

Über eure studentische Emailadresse werdet ihr über Veranstaltungen und andere wichtige Themen auf dem Laufenden gehalten. Außerdem gibt es einen Telegram-Channel auf dem wir euch auf dem Laufenden halten mit einem angegliedertem Diskussionraum der mehr Interaktion als Email ermöglicht (In der App könnt ihr nach infounigoe suchen). 

Jede:r Studierende der:die sich für mehr Abwechselung neben dem Informatikstudium einsetzen möchte, gerne etwas verändern will oder auch nur mal rein schauen möchte, ist herzlich eingeladen, bei unseren [zweiwöchentlichen Treffen](slug:fgtreffen) vorbei zuschauen. Wir treffen uns im Raum 0.101 im IfI in einer offenen Runde, wärend des Semesters aktuell dienstags in ungeraden Kalenderwochen um 18 Uhr.

Wir sind auch jederzeit per E-Mail erreichbar unter: [fachgruppe@informatik.uni-goettingen.de](mailto:fachgruppe@informatik.uni-goettingen.de)
Aktuell ist Robin Giese unser Fachgruppensprecher.

Wir freuen uns auf euch! Eure Fachgruppe Informatik
