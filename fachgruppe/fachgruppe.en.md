---
title: "About us"
slug: fachgruppe

---

We are a group of dedicated students from the area of computer science. We organize different events every semester, like the orientation week, Christmas lectures, or the [Cookie-Talks](slug:cookie-talks). Aside from networking, the quality of studies in computer science is an important concern. We do not only rely on our own experience, but also get feedback from you all the time. If you have a suggestion, feel free to send us an e-mail or visit our weekly meetups.
Some of us are also part of university committees, e.g. the academic commission, which specifically deals with the quality of studies, or the examination board, which also deals with hardship cases.

You will be kept up to date on events and other important topics via your student email address. There is also a Telegram channel where we keep you updated with an attached discussion room that allows more interaction than email (In the app you can search for infounigoe). 

Any student who would like to advocate for more variety aside from studying computer science, who would like to make a difference, or who would just like to drop in is welcome to stop by our [biweekly meetings](slug:fgtreffen). We meet in room 0.101 in the Institute of Computer Science in an open round, during the semester currently every odd numbered week at 6 pm.

We can also be reached by email at any time at: [fachgruppe@informatik.uni-goettingen.de](mailto:fachgruppe@informatik.uni-goettingen.de)
Currently Robin Giese is our speaker.

We are looking forward to meeting you! Your Fachgruppe Computer Science


